##Requirement:
####Build a TODO list app for iOS. The app should work as follows:
	1. users create an account via regular sign up (email and password) or login with Facebook
	2. they can create / delete a TODO item
	3. they can mark an item as completed
	4. they can view their list of TODO items
	5. offline support - persist data on the local storage
	6. online support - synchronize date to/from a remote location
	7. they may view a chart displaying statistics about the TODO items
	8. the application should have a nice clean design, with animations and pleasant colors
	9. the users may connect with other users and view their list of TODO items, without having the ability to edit or delete them

####Functionalities:
1. The user can log in.
2. The user can log in with Facebook. 
3. The user can see the list of ToDo items.
4. The user can create a ToDo item.
5. The user can delete a ToDo item.
6. The user can mark a ToDo item as completed.
7. The user may view a chart displaying statistics about the TODO items.
8. The user may connect with another one, each one having read access to each other’s list on TODO items.
9. The user can log out.