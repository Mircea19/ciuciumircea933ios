//
//  DetailViewController.swift
//  ToDoApp
//
//  Created by Mircea Ciuciu on 11/8/16.
//  Copyright © 2016 Mircea Ciuciu. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet weak var editField: UITextField!
    @IBOutlet weak var checkedSwitch: UISwitch!

    func configureView() {
        
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.detailDescriptionLabel {
                editField.text = detail.itemName
                if detail.completed{
                    checkedSwitch.setOn(true, animated: true)
                }
                else{
                    checkedSwitch.setOn(false, animated: true)
                }
            }
        }
    }
    
    
    @IBAction func saveItem(_ sender: AnyObject) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: ToDo? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }


}

