//
//  ToDo.swift
//  ToDoApp
//
//  Created by Mircea Ciuciu on 11/8/16.
//  Copyright © 2016 Mircea Ciuciu. All rights reserved.
//

import Foundation

class ToDo {
    
    var itemName: String=""
    var completed:Bool=false
    var id:Int = 0
    
    init(itemName: String, completed: Bool, id: Int) {
        self.itemName = itemName
        self.completed = completed
        self.id  = id
    }
    
}
