Requirement:
Build a TODO list app for iPhone. The app should work as follows:
	- users create an account via regular sign up (email and password) or login with Facebook
	- they can create / delete a TODO item
	- they can mark an item as completed
	- they can view their list of TODO items
	- offline support - persist data on the local storage
	- online support - synchronize date to/from a remote location
	- they may view a chart displaying statistics about the TODO items
	- the application should have a nice clean design, with animations and pleasant colors
	- they may connect with other users and view their list of TODO items, without having the ability to edit or delete them

Functionalities:
F1. The user can log in.
F2. The user can log in with Facebook.
F3. The user can see the list of ToDo items.
F4. The user can create a ToDo item.
F5. The user can delete a ToDo item.
F6. The user can mark a ToDo item as completed.
F7. The user may view a chart displaying statistics about the TODO items.
F8. The user may connect with another one, each one having read access to each other’s list on TODO items.
F9. The user can log out.


